package programmer.zaman.now.kotlin.restful

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.RequestBuilder
import org.springframework.test.web.servlet.htmlunit.webdriver.MockMvcHtmlUnitDriverBuilder
import programmer.zaman.now.kotlin.restful.controller.CustomerController
import programmer.zaman.now.kotlin.restful.entity.Customer
import programmer.zaman.now.kotlin.restful.model.request.CustomerRequest
import programmer.zaman.now.kotlin.restful.service.CustomerService


@WebMvcTest(CustomerController::class)
class CustomerTests(@Autowired val mockMvc: MockMvc) {

    @MockBean
    lateinit var customerService: CustomerService

    @Test
    fun createCustomer(){
        val customerRequest = CustomerRequest(
            first_name = "Khalif",
            last_name = "Al Khawarizmi",
            email = "khawa@titipbeliin.com",
            phone_number = "09982827",
            password = "12345"
        )

        val customerResponse = customerService.create(customerRequest)

//        mockMvc.perform(post)

    }
}