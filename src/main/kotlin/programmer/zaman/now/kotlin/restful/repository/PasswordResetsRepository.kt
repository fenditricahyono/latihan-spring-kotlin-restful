package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.PasswordResets
import org.springframework.data.jpa.repository.JpaRepository


interface PasswordResetsRepository : JpaRepository<PasswordResets, String>
