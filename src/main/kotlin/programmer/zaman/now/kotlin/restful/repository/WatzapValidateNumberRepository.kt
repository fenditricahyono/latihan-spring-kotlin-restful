package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.WatzapValidateNumber
import org.springframework.data.jpa.repository.JpaRepository


interface WatzapValidateNumberRepository : JpaRepository<WatzapValidateNumber, Long>
