package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Expense
import org.springframework.data.jpa.repository.JpaRepository


interface ExpenseRepository : JpaRepository<Expense, Long>
