package programmer.zaman.now.kotlin.restful.service

import org.springframework.security.core.userdetails.UserDetailsService
import programmer.zaman.now.kotlin.restful.model.request.*
import programmer.zaman.now.kotlin.restful.model.response.CityResponse
import programmer.zaman.now.kotlin.restful.model.response.CustomerResponse
import programmer.zaman.now.kotlin.restful.model.response.ProductResponse

interface CustomerService: UserDetailsService {

    fun create(customerRequest: CustomerRequest): CustomerResponse

    fun get(uuid: String): CustomerResponse

    fun validate(loginRequest: LoginRequest): CustomerResponse?

    fun update(uuid: String, customerRequest: CustomerRequest): CustomerResponse

    fun delete(uuid: String): Boolean

    fun list(otoPaginationRequestList: OtoPaginationRequestList): List<CustomerResponse>
}