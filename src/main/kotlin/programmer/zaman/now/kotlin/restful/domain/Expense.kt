package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.LocalDate
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Expense {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        unique = true
    )
    var uuid: String? = null

    @Column(
        nullable = false,
        name = "\"description\""
    )
    var description: String? = null

    @Column(nullable = false)
    var expenseDate: LocalDate? = null

    @Column(nullable = false)
    var totalBill: Int? = null

    @Column(nullable = false)
    var totalPayment: Int? = null

    @Column
    var note: String? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "bus_id",
        nullable = false
    )
    var bus: Bus? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "expense_category_id",
        nullable = false
    )
    var expenseCategory: ExpenseCategory? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "input_by_id",
        nullable = false
    )
    var inputBy: Users? = null

    @OneToMany(mappedBy = "expense")
    var expenseExpensePayments: MutableSet<ExpensePayment>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
