package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Setting
import org.springframework.data.jpa.repository.JpaRepository


interface SettingRepository : JpaRepository<Setting, Long>
