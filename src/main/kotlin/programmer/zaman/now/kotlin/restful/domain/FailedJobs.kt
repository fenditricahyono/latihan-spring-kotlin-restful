package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class FailedJobs {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var connection: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var queue: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var payload: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var exception: String? = null

    @Column(nullable = false)
    var failedAt: OffsetDateTime? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
