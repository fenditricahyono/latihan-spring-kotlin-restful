package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BookingPassenger
import org.springframework.data.jpa.repository.JpaRepository


interface BookingPassengerRepository : JpaRepository<BookingPassenger, Long>
