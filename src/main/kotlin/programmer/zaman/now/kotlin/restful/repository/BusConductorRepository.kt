package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusConductor
import org.springframework.data.jpa.repository.JpaRepository


interface BusConductorRepository : JpaRepository<BusConductor, Long>
