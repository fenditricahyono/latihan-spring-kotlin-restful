package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class TripSubroute {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        unique = true,
        length = 50
    )
    var uuid: String? = null

    @Column(nullable = false)
    var departureTime: OffsetDateTime? = null

    @Column(nullable = false)
    var arrivedTime: OffsetDateTime? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "trip_id",
        nullable = false
    )
    var trip: Trip? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "departure_city_id",
        nullable = false
    )
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "destination_city_id",
        nullable = false
    )
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id")
    var route: Route? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
