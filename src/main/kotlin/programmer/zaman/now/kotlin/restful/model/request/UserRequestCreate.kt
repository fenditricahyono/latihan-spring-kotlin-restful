package programmer.zaman.now.kotlin.restful.model.request

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class UserRequestCreate(

        @field:NotBlank
        val username: String?,

        @field:NotBlank
        val password: String?,

        @field:NotBlank
        val fullname: String?

)