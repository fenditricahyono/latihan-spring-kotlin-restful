package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.RouteBoardingPoint
import org.springframework.data.jpa.repository.JpaRepository


interface RouteBoardingPointRepository : JpaRepository<RouteBoardingPoint, Long>
