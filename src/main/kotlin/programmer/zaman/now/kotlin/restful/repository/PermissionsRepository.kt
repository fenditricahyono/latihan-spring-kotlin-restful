package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Permissions
import org.springframework.data.jpa.repository.JpaRepository


interface PermissionsRepository : JpaRepository<Permissions, Long>
