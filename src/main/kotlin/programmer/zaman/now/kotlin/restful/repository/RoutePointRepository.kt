package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.RoutePoint
import org.springframework.data.jpa.repository.JpaRepository


interface RoutePointRepository : JpaRepository<RoutePoint, Long>
