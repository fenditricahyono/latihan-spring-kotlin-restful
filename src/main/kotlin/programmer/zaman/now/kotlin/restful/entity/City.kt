package programmer.zaman.now.kotlin.restful.entity

import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Past

@Entity
@Table(name = "city")
data class City(

    @Id
    @Column(name = "code", length = 5)
    val code: String,

    @Column(name = "uuid", length = 75, unique = true)
    val uuid: String,

    @Column(name = "city_name")
    var cityName: String,

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    val createdAt: Date,

    @Column(name = "updated_at", columnDefinition = "timestamp with time zone")
    var updatedAt: Date?,

    @Column(name = "deleted_at", insertable = false, columnDefinition = "timestamp with time zone")
    var deletedAt: Date?
)