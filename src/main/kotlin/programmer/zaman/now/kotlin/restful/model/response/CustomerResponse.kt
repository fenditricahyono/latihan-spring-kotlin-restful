package programmer.zaman.now.kotlin.restful.model.response

import java.util.Date

data class CustomerResponse(
    val uuid: String,
    val email: String,
    val phone_number: String,
    val first_name: String,
    val last_name: String,
    val created_at: Date,
    val updated_at: Date?,
    val deleted_at: Date?,
    val api_key: ApiKeyResponse? = null
)
