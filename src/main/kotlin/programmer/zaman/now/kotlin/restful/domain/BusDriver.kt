package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class BusDriver {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 20)
    var driverName: String? = null

    @Column(length = 32)
    var phoneNumber: String? = null

    @Column
    var address: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "driver")
    var driverBuss: MutableSet<Bus>? = null

    @OneToMany(mappedBy = "driverId2")
    var driverId2Buss: MutableSet<Bus>? = null

    @OneToMany(mappedBy = "driver")
    var driverTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "driverId2")
    var driverId2Trips: MutableSet<Trip>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
