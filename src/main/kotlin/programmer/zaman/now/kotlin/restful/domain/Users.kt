package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Users {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 72)
    var name: String? = null

    @Column(
        unique = true,
        length = 50
    )
    var username: String? = null

    @Column(
        unique = true,
        length = 50
    )
    var email: String? = null

    @Column(length = 200)
    var password: String? = null

    @Column(length = 100)
    var apiToken: String? = null

    @Column(length = 150)
    var rememberToken: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var emailVerifiedAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(length = 500)
    var avatar: String? = null

    @OneToMany(mappedBy = "operator")
    var operatorBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "operatorCreate")
    var operatorCreateCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "operatorReceive")
    var operatorReceiveCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "inputBy")
    var inputByExpenses: MutableSet<Expense>? = null

    @OneToMany(mappedBy = "inputBy")
    var inputByExpensePayments: MutableSet<ExpensePayment>? = null

    @OneToMany(mappedBy = "operator")
    var operatorInvoices: MutableSet<Invoice>? = null

    @OneToMany(mappedBy = "operator")
    var operatorPayments: MutableSet<Payment>? = null

    @OneToMany(mappedBy = "operator")
    var operatorSettings: MutableSet<Setting>? = null

    @OneToMany(mappedBy = "operator")
    var operatorTicketPriceSchedulingGroups: MutableSet<TicketPriceSchedulingGroup>? = null

    @OneToMany(mappedBy = "operator")
    var operatorTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @OneToMany(mappedBy = "operator")
    var operatorTripCosts: MutableSet<TripCost>? = null

    @OneToMany(mappedBy = "user")
    var userUploads: MutableSet<Upload>? = null

    @OneToMany(mappedBy = "operator")
    var operatorUserCashDeposits: MutableSet<UserCashDeposit>? = null

    @OneToMany(mappedBy = "verifyBy")
    var verifyByUserCashDeposits: MutableSet<UserCashDeposit>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agency_code_id")
    var agencyCode: Agency? = null

    @OneToMany(mappedBy = "user")
    var userUserActivitys: MutableSet<UserActivity>? = null

    @OneToMany(mappedBy = "operator")
    var operatorUserCashHistorys: MutableSet<UserCashHistory>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
