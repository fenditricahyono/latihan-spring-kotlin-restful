package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TripTemplate
import org.springframework.data.jpa.repository.JpaRepository


interface TripTemplateRepository : JpaRepository<TripTemplate, Long>
