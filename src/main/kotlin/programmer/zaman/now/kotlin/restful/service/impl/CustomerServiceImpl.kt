package programmer.zaman.now.kotlin.restful.service.impl

import org.springframework.http.ResponseEntity
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import programmer.zaman.now.kotlin.restful.entity.ApiKey
import programmer.zaman.now.kotlin.restful.entity.AppUserRole
import programmer.zaman.now.kotlin.restful.entity.Customer
import programmer.zaman.now.kotlin.restful.error.NotFoundException
import programmer.zaman.now.kotlin.restful.model.request.CustomerRequest
import programmer.zaman.now.kotlin.restful.model.request.LoginRequest
import programmer.zaman.now.kotlin.restful.model.request.OtoPaginationRequestList
import programmer.zaman.now.kotlin.restful.model.response.ApiKeyResponse
import programmer.zaman.now.kotlin.restful.model.response.CustomerResponse
import programmer.zaman.now.kotlin.restful.model.response.Message
import programmer.zaman.now.kotlin.restful.repository.ApiKeyRepository
import programmer.zaman.now.kotlin.restful.repository.CustomerRepository
import programmer.zaman.now.kotlin.restful.service.CustomerService
import programmer.zaman.now.kotlin.restful.validation.ValidationUtil
import java.util.*

@Service
class CustomerServiceImpl(
    val customerRepository: CustomerRepository,
    val apiKeyRepository: ApiKeyRepository,
    val validationUtil: ValidationUtil
):CustomerService {
    override fun create(customerRequest: CustomerRequest): CustomerResponse {
        val validationUtil1 = validationUtil.validate(customerRequest)
        val encoder = BCryptPasswordEncoder(16)
//        val result: String = encoder.encode(customerRequest.password)

        val customer = Customer(
            uuid = "customer-".plus(UUID.randomUUID().toString()),
            email = customerRequest.email,
            firstName = customerRequest.first_name,
            lastName = customerRequest.last_name,
            phoneNumber = customerRequest.phone_number,
            createdAt = Date(),
            updatedAt = null
        )
        customer.appUserRole = AppUserRole.ADMIN
        customer.password = customerRequest.password
        customerRepository.save(customer)

        val apiKey = ApiKey(
                            customer = customer,
                            id = UUID.randomUUID().toString()
                        )
        apiKeyRepository.save(apiKey)

        return CustomerResponse(
            uuid = customer.uuid,
            email = customer.email,
            first_name = customer.firstName,
            last_name = customer.lastName,
            phone_number = customer.phoneNumber,
            created_at = customer.createdAt,
            updated_at = customer.updatedAt,
            deleted_at = customer.deletedAt,
            api_key = ApiKeyResponse(
                token = apiKey.id,
                createdAt = apiKey.createdAt
            )
        )
    }

    override fun get(uuid: String): CustomerResponse {
        val customer = customerRepository.findByUuid(uuid)
        if(customer != null)
            return  CustomerResponse(
                uuid = customer.uuid,
                email = customer.email,
                first_name = customer.firstName,
                last_name = customer.lastName,
                phone_number = customer.phoneNumber,
                created_at = customer.createdAt,
                updated_at = customer.updatedAt,
                deleted_at = customer.deletedAt
            )
        else
            throw NotFoundException()
    }

    override fun validate(loginRequest: LoginRequest): CustomerResponse? {
        val customer = customerRepository.findByEmail(loginRequest.email)

        if (!customer?.comparePassword(loginRequest.password)!!) {
            throw NotFoundException()
        }else{

            return CustomerResponse(
                uuid = customer.uuid,
                email = customer.email,
                first_name = customer.firstName,
                last_name = customer.lastName,
                phone_number = customer.phoneNumber,
                created_at = customer.createdAt,
                updated_at = customer.updatedAt,
                deleted_at = customer.deletedAt
            )
        }
    }

    override fun update(uuid: String, customerRequest: CustomerRequest): CustomerResponse {
        TODO("Not yet implemented")
    }

    override fun delete(uuid: String): Boolean {
        TODO("Not yet implemented")
    }

    override fun list(otoPaginationRequestList: OtoPaginationRequestList): List<CustomerResponse> {
        TODO("Not yet implemented")
    }

    override fun loadUserByUsername(email: String): Customer? {
        return customerRepository.findByEmail(email)
    }
}