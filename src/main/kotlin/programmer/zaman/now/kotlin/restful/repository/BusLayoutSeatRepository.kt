package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusLayoutSeat
import org.springframework.data.jpa.repository.JpaRepository


interface BusLayoutSeatRepository : JpaRepository<BusLayoutSeat, Long>
