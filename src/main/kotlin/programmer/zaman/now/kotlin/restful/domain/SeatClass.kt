package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class SeatClass {

    @Id
    @Column(
        nullable = false,
        updatable = false,
        length = 30
    )
    var classCode: String? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 50)
    var seatClass: String? = null

    @Column(length = 10)
    var color: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "classCode")
    var classCodeBookingPassengers: MutableSet<BookingPassenger>? = null

    @OneToMany(mappedBy = "classCode")
    var classCodeBusLayoutSeats: MutableSet<BusLayoutSeat>? = null

    @OneToMany(mappedBy = "classCode")
    var classCodeRouteClassPrices: MutableSet<RouteClassPrice>? = null

    @OneToMany(mappedBy = "classCode")
    var classCodeTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
