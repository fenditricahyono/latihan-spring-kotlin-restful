package programmer.zaman.now.kotlin.restful.model.request

data class ProductRequestList(

        val page: Int,

        val size: Int

)