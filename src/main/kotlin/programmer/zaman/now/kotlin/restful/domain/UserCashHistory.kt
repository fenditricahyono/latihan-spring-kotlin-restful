package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class UserCashHistory {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(
        nullable = false,
        name = "\"description\"",
        columnDefinition = "longtext"
    )
    var description: String? = null

    @Column(nullable = false)
    var transactionValue: Int? = null

    @Column(nullable = false)
    var balance: Int? = null

    @Column(nullable = false)
    var contentType: String? = null

    @Column(
        nullable = false,
        length = 32
    )
    var contentId: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "agency_code_id",
        nullable = false
    )
    var agencyCode: Agency? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_cash_deposit_id")
    var userCashDeposit: UserCashDeposit? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "operator_id",
        nullable = false
    )
    var `operator`: Users? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "child_id")
    var child: UserCashHistory? = null

    @OneToMany(mappedBy = "child")
    var childUserCashHistorys: MutableSet<UserCashHistory>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
