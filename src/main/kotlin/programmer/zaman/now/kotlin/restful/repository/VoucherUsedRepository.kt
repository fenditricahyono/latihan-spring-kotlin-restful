package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.VoucherUsed
import org.springframework.data.jpa.repository.JpaRepository


interface VoucherUsedRepository : JpaRepository<VoucherUsed, Long>
