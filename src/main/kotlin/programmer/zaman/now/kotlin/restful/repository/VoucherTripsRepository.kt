package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.VoucherTrips
import org.springframework.data.jpa.repository.JpaRepository


interface VoucherTripsRepository : JpaRepository<VoucherTrips, Long>
