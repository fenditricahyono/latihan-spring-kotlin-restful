package programmer.zaman.now.kotlin.restful.entity

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "customer")
class Customer(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Int? = null,

    @Column(name = "uuid", length = 70, unique = true)
    val uuid: String,

    @Column(name = "email", length = 150, unique = true)
    val email: String,

    @Column(name = "phone_number", length = 32, unique = true)
    val phoneNumber: String,

    @Enumerated(EnumType.STRING)
    @Column(name="user_role")
    var appUserRole: AppUserRole? = null,

    @Column(name = "first_name", length = 32)
    val firstName: String,

    @Column(name = "last_name", length = 32)
    val lastName: String,

    @Column(name = "created_at")
    var createdAt: Date,

    @Column(name = "updated_at")
    var updatedAt: Date? = null,

    @Column(name = "deleted_at")
    var deletedAt: Date? = null
): UserDetails{

    @Column(name = "password")
    private var password = ""
    override fun getPassword(): String {
        return password
    }
    fun setPassword(value: String) {
        val passwordEncoder = BCryptPasswordEncoder(16)
        password = passwordEncoder.encode(value)
    }

    public fun comparePassword(password: String): Boolean {
        return BCryptPasswordEncoder().matches(password, this.password)
    }

    override fun getAuthorities(): Collection<out GrantedAuthority> {
        val authority = SimpleGrantedAuthority(appUserRole?.name)
        return Collections.singletonList(authority)
    }

    override fun getUsername(): String {
        return username
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun isEnabled(): Boolean {
        return true
    }
}
