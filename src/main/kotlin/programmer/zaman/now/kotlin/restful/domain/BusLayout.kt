package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class BusLayout {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 100)
    var layoutName: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "busLayout")
    var busLayoutBusLayoutSeats: MutableSet<BusLayoutSeat>? = null

    @OneToMany(mappedBy = "busLayout")
    var busLayoutBusNumbers: MutableSet<BusNumber>? = null

    @OneToMany(mappedBy = "busLayoutSecond")
    var busLayoutSecondBusNumbers: MutableSet<BusNumber>? = null

    @OneToMany(mappedBy = "busLayout")
    var busLayoutTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "busLayoutSecond")
    var busLayoutSecondTrips: MutableSet<Trip>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
