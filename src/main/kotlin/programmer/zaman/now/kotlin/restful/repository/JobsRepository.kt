package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Jobs
import org.springframework.data.jpa.repository.JpaRepository


interface JobsRepository : JpaRepository<Jobs, Long>
