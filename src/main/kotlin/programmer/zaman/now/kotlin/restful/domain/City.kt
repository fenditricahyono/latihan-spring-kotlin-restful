package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class City {

    @Id
    @Column(
        nullable = false,
        updatable = false,
        length = 32
    )
    var code: String? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 100)
    var cityName: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "cityCode")
    var cityCodeAgencys: MutableSet<Agency>? = null

    @OneToMany(mappedBy = "cityCode")
    var cityCodeBoardingPoints: MutableSet<BoardingPoint>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityBookingPassengers: MutableSet<BookingPassenger>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityBookingPassengers: MutableSet<BookingPassenger>? = null

    @OneToMany(mappedBy = "cityCode")
    var cityCodeBookingTripTrackSeats: MutableSet<BookingTripTrackSeat>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityRoutes: MutableSet<Route>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityRoutes: MutableSet<Route>? = null

    @OneToMany(mappedBy = "cityCode")
    var cityCodeRouteTracks: MutableSet<RouteTrack>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "departureCity")
    var departureCityTripSubroutes: MutableSet<TripSubroute>? = null

    @OneToMany(mappedBy = "destinationCity")
    var destinationCityTripSubroutes: MutableSet<TripSubroute>? = null

    @OneToMany(mappedBy = "cityCode")
    var cityCodeTripTracks: MutableSet<TripTrack>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
