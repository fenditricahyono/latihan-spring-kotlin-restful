package programmer.zaman.now.kotlin.restful.repository

import org.springframework.data.jpa.repository.JpaRepository
import programmer.zaman.now.kotlin.restful.entity.Product
import programmer.zaman.now.kotlin.restful.entity.ProductSpecification

interface ProductSpecificationRepository : JpaRepository<ProductSpecification, String>{

}