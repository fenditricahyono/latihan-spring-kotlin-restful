package programmer.zaman.now.kotlin.restful.controller

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import programmer.zaman.now.kotlin.restful.model.WebResponse
import programmer.zaman.now.kotlin.restful.model.request.CustomerRequest
import programmer.zaman.now.kotlin.restful.model.request.LoginRequest
import programmer.zaman.now.kotlin.restful.model.request.OtoPaginationRequestList
import programmer.zaman.now.kotlin.restful.model.response.CustomerResponse
import programmer.zaman.now.kotlin.restful.model.response.Message
import programmer.zaman.now.kotlin.restful.service.CustomerService
import java.util.Date
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletResponse

@RestController
class CustomerController(val customerService: CustomerService) {

    @GetMapping(
        value = ["/api/customers"],
        produces = ["application/json"]
    )
    fun list(@RequestParam(value = "size", defaultValue = "20") size: Int,
                     @RequestParam(value = "page", defaultValue = "0") page: Int): WebResponse<List<CustomerResponse>> {

        val request = OtoPaginationRequestList(page = page, size = size)
        val responses = customerService.list(request)
        return WebResponse(
            code = 200,
            status = "OK",
            data = responses
        )
    }

    @PostMapping(
        value = ["api/customer"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun create(@RequestBody customerRequest: CustomerRequest): WebResponse<CustomerResponse>{
        val customerResponse = customerService.create(customerRequest)
        return WebResponse(
            code = 201,
            status = "OK",
            data = customerResponse
        )
    }

    @PutMapping(
        value = ["api/customer/{uuid}"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun update(@PathVariable uuid:String, @RequestBody customerRequest: CustomerRequest): WebResponse<CustomerResponse>{
        val customerResponse = customerService.update(uuid, customerRequest)
        return WebResponse(
            code = 201,
            status = "OK",
            data = customerResponse
        )
    }

    @DeleteMapping(
        value = ["api/customer/{uuid}"],
        produces = ["application/json"]
    )
    fun delete(@PathVariable code: String): WebResponse<String>{
        val isSuccess = customerService.delete(code)
        return WebResponse(
            code = 200,
            status = "OK",
            data = if(isSuccess) "Data berhasil dihapus" else "Data gagal dihapus"
        )
    }

    @PostMapping("api/customer/login")
    fun login(@RequestBody body: LoginRequest, response: HttpServletResponse): ResponseEntity<Any> {
        val customer = this.customerService.validate(body)
            ?: return ResponseEntity.badRequest().body(Message("user not found!"))

        val issuer = customer.uuid.toString()

        val jwt = Jwts.builder()
            .setIssuer(issuer)
            .setExpiration(Date(System.currentTimeMillis() + 60 * 24 * 1000)) // 1 day
            .signWith(SignatureAlgorithm.HS512, "secret").compact()

        val cookie = Cookie("jwt", jwt)
        cookie.isHttpOnly = true

        response.addCookie(cookie)

        return ResponseEntity.ok(Message("success"))
    }

    @GetMapping("user")
    fun user(@CookieValue("jwt") jwt: String?): ResponseEntity<Any> {
        try {
            if (jwt == null) {
                return ResponseEntity.status(401).body(Message("unauthenticated"))
            }

            val body = Jwts.parser().setSigningKey("secret").parseClaimsJws(jwt).body

            return ResponseEntity.ok(this.customerService.get(body.issuer.toString()))
        } catch (e: Exception) {
            return ResponseEntity.status(401).body(Message("unauthenticated"))
        }
    }

    @PostMapping("logout")
    fun logout(response: HttpServletResponse): ResponseEntity<Any> {
        val cookie = Cookie("jwt", "")
        cookie.maxAge = 0

        response.addCookie(cookie)

        return ResponseEntity.ok(Message("success"))
    }
}