package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TripTrack
import org.springframework.data.jpa.repository.JpaRepository


interface TripTrackRepository : JpaRepository<TripTrack, Long>
