package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class BookingPassenger {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column
    var customerId: Long? = null

    @Column
    var `operator`: Long? = null

    @Column
    var seatNumber: Int? = null

    @Column(nullable = false)
    var price: Int? = null

    @Column(length = 150)
    var passengerName: String? = null

    @Column
    var floor: Int? = null

    @Column(
        nullable = false,
        length = 100
    )
    var accessToken: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_city_id")
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_city_id")
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_layout_seat_id")
    var busLayoutSeat: BusLayoutSeat? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_code_id")
    var classCode: SeatClass? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "booking_id")
    var booking: Booking? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "trip_id",
        nullable = false
    )
    var trip: Trip? = null

    @OneToMany(mappedBy = "passenger")
    var passengerBookingTripTrackSeats: MutableSet<BookingTripTrackSeat>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
