package programmer.zaman.now.kotlin.restful.model.response

import java.util.*

data class ProductResponse(

        val id: String,

        val name: String,

        val price: Long,

        val quantity: Int,

        val createdAt: Date,

        val updatedAt: Date?,

        val specifications: List<ProductSpecificationResponse>

)