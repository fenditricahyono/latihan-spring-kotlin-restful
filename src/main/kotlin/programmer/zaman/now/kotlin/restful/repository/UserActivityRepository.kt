package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.UserActivity
import org.springframework.data.jpa.repository.JpaRepository


interface UserActivityRepository : JpaRepository<UserActivity, Long>
