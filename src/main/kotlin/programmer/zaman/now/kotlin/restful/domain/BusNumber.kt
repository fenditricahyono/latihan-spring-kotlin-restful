package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class BusNumber {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 50)
    var busNumber: String? = null

    @Column(
        name = "\"description\"",
        length = 150
    )
    var description: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberBuss: MutableSet<Bus>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_layout_id")
    var busLayout: BusLayout? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_layout_second_id")
    var busLayoutSecond: BusLayout? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberTicketPriceSchedulingGroups: MutableSet<TicketPriceSchedulingGroup>? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberTripTemplates: MutableSet<TripTemplate>? = null

    @OneToMany(mappedBy = "busNumber")
    var busNumberVoucherBusNumberss: MutableSet<VoucherBusNumbers>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
