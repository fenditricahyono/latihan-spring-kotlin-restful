package programmer.zaman.now.kotlin.restful.controller

import org.springframework.web.bind.annotation.*
import programmer.zaman.now.kotlin.restful.error.DuplicateException
import programmer.zaman.now.kotlin.restful.model.WebResponse
import programmer.zaman.now.kotlin.restful.model.request.CityRequest
import programmer.zaman.now.kotlin.restful.model.request.OtoPaginationRequestList
import programmer.zaman.now.kotlin.restful.model.response.CityResponse
import programmer.zaman.now.kotlin.restful.service.CityService

@RestController
class CityController(val cityService: CityService) {

    @GetMapping(
        value = ["/api/cities"],
        produces = ["application/json"]
    )
    fun list(@RequestParam(value = "size", defaultValue = "20") size: Int,
                     @RequestParam(value = "page", defaultValue = "0") page: Int): WebResponse<List<CityResponse>> {

        val request = OtoPaginationRequestList(page = page, size = size)
        val responses = cityService.list(request)
        return WebResponse(
            code = 200,
            status = "OK",
            data = responses
        )
    }

    @PostMapping(
        value = ["api/city"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun create(@RequestBody cityRequest: CityRequest): WebResponse<CityResponse>{
        val cityResponse = cityService.create(cityRequest)
        return WebResponse(
            code = 201,
            status = "OK",
            data = cityResponse
        )
    }

    @PutMapping(
        value = ["api/city/{code}"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    fun update(@PathVariable code:String, @RequestBody cityRequest: CityRequest): WebResponse<CityResponse>{
        val cityResponse = cityService.update(code, cityRequest)
        return WebResponse(
            code = 201,
            status = "OK",
            data = cityResponse
        )
    }

    @DeleteMapping(
        value = ["api/city/{code}"],
        produces = ["application/json"]
    )
    fun delete(@PathVariable code: String): WebResponse<String>{
        val isSuccess = cityService.delete(code)
        return WebResponse(
            code = 200,
            status = "OK",
            data = if(isSuccess) "Data berhasil dihapus" else "Data gagal dihapus"
        )
    }
}