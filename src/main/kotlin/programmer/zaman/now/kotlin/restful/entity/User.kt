package programmer.zaman.now.kotlin.restful.entity

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "users")
class User(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        @Column(name = "fullname")
        var fullname: String,

        @Column(name = "username")
        private var username: String,

        @Enumerated(EnumType.STRING)
        @Column(name="user_role")
        var appUserRole: AppUserRole,

        @Column(name = "created_at")
        var createdAt: Date,

        @Column(name = "updated_at")
        var updatedAt: Date?
): UserDetails{


        @Column(name = "password")
        private var password = ""
        override fun getPassword(): String {
                return password
        }
        fun setPassword(value: String) {
                val passwordEncoder = BCryptPasswordEncoder(16)
                password = passwordEncoder.encode(value)
        }

        fun comparePassword(password: String): Boolean {
                return BCryptPasswordEncoder().matches(password, this.password)
        }

        override fun getAuthorities(): Collection<out GrantedAuthority> {
                val authority = SimpleGrantedAuthority(appUserRole.name)
                return Collections.singletonList(authority)
        }

        override fun getUsername(): String {
                return username
        }

        override fun isAccountNonExpired(): Boolean {
                return true
        }

        override fun isAccountNonLocked(): Boolean {
                return true
        }

        override fun isCredentialsNonExpired(): Boolean {
                return true
        }

        override fun isEnabled(): Boolean {
                return true
        }
}