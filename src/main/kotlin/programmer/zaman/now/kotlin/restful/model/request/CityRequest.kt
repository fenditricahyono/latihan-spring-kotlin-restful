package programmer.zaman.now.kotlin.restful.model.request

import javax.validation.constraints.NotBlank

data class CityRequest(

    @field: NotBlank
    val code: String,

    @field: NotBlank
    val city_name: String

)
