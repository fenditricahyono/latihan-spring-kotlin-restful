package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Country
import org.springframework.data.jpa.repository.JpaRepository


interface CountryRepository : JpaRepository<Country, Long>
