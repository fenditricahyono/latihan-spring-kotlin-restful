package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Invoice {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(
        nullable = false,
        unique = true
    )
    var invoiceNumber: Long? = null

    @Column(
        nullable = false,
        unique = true,
        length = 50
    )
    var invoiceCode: String? = null

    @Column(length = 50)
    var accountName: String? = null

    @Column(length = 100)
    var accountBank: String? = null

    @Column(length = 100)
    var accountNumber: String? = null

    @Column(nullable = false)
    var discount: Int? = null

    @Column
    var totalBill: Int? = null

    @Column(nullable = false)
    var totalRefund: Int? = null

    @Column(nullable = false)
    var totalRefundCharge: Int? = null

    @Column(nullable = false)
    var totalPayment: Int? = null

    @Column(columnDefinition = "longtext")
    var additionalInfo: String? = null

    @Column
    var expiredAt: OffsetDateTime? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "invoice")
    var invoiceBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "invoice")
    var invoiceCargos: MutableSet<Cargo>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    var customer: Customer? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voucher_id")
    var voucher: Voucher? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_id")
    var `operator`: Users? = null

    @OneToMany(mappedBy = "invoice")
    var invoiceInvoiceLogs: MutableSet<InvoiceLog>? = null

    @OneToMany(mappedBy = "invoice")
    var invoicePayments: MutableSet<Payment>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
