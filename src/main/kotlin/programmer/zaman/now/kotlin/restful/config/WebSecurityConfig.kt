package programmer.zaman.now.kotlin.restful.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import programmer.zaman.now.kotlin.restful.service.CustomerService

//@Configuration
//@EnableWebSecurity
class WebSecurityConfig(
    @Autowired
    val customerService: CustomerService
): WebSecurityConfigurerAdapter() {
//    val bCryptPasswordEncoder: BCryptPasswordEncoder
//        get() {
//            TODO()
//        }
    @Bean
    fun passwordEncoder(): BCryptPasswordEncoder? {
        return BCryptPasswordEncoder()
}
    override fun configure(http: HttpSecurity?) {
        print("test configgggg ------------------ ")
        http?.csrf()?.disable()?.authorizeRequests()?.antMatchers("/api/customer")?.permitAll()
            ?.anyRequest()?.fullyAuthenticated()?.and()?.httpBasic()
    }
    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.authenticationProvider(daoAuthenticationProvider())
    }

    fun daoAuthenticationProvider(): DaoAuthenticationProvider{
        val provider  = DaoAuthenticationProvider()
        provider.setPasswordEncoder(passwordEncoder())
        print(customerService)
        print("halooooo")
        provider.setUserDetailsService(customerService)
        return provider
    }
}