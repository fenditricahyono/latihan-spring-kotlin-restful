package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class BusLayoutSeat {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 50)
    var seatNumber: String? = null

    @Column
    var coordinateX: Int? = null

    @Column
    var coordinateY: Int? = null

    @Column(nullable = false)
    var seatName: String? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "busLayoutSeat")
    var busLayoutSeatBookingPassengers: MutableSet<BookingPassenger>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "class_code_id")
    var classCode: SeatClass? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_layout_id")
    var busLayout: BusLayout? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
