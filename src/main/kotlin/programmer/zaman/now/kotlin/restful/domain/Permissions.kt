package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.JoinTable
import jakarta.persistence.ManyToMany
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Permissions {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        length = 50
    )
    var uuid: String? = null

    @Column(nullable = false)
    var name: String? = null

    @Column(nullable = false)
    var guardName: String? = null

    @Column(
        nullable = false,
        name = "\"description\"",
        length = 150
    )
    var description: String? = null

    @Column
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "permission")
    var permissionModelHasPermissionss: MutableSet<ModelHasPermissions>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "permission_group_id",
        nullable = false
    )
    var permissionGroup: PermissionGroup? = null

    @ManyToMany
    @JoinTable(
        name = "role_has_permissions",
        joinColumns = [
            JoinColumn(name = "permissions_id")
        ],
        inverseJoinColumns = [
            JoinColumn(name = "roles_id")
        ]
    )
    var roleHasPermissionsRoless: MutableSet<Roles>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
