package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ExpenseCategory
import org.springframework.data.jpa.repository.JpaRepository


interface ExpenseCategoryRepository : JpaRepository<ExpenseCategory, Long>
