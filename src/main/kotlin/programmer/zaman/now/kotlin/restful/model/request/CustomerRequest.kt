package programmer.zaman.now.kotlin.restful.model.request

import javax.validation.constraints.NotBlank

data class CustomerRequest(

    @field: NotBlank
    val email: String,

    val password: String,

    @field: NotBlank
    val phone_number: String,

    @field: NotBlank
    val first_name: String,

    @field: NotBlank
    val last_name: String

)
