package programmer.zaman.now.kotlin.restful.service

import programmer.zaman.now.kotlin.restful.model.request.ProductRequestCreate
import programmer.zaman.now.kotlin.restful.model.request.ProductRequestList
import programmer.zaman.now.kotlin.restful.model.response.ProductResponse
import programmer.zaman.now.kotlin.restful.model.request.UpdateProductRequest
import programmer.zaman.now.kotlin.restful.model.request.UserRequestCreate

interface UserService {

    fun create(userRequestCreate: UserRequestCreate): ProductResponse

    fun get(id: Int): ProductResponse

    fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse

    fun delete(id: String)

    fun list(listProductRequest: ProductRequestList): List<ProductResponse>

}