package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Captcha
import org.springframework.data.jpa.repository.JpaRepository


interface CaptchaRepository : JpaRepository<Captcha, Long>
