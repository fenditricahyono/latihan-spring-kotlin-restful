package programmer.zaman.now.kotlin.restful.config

import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import programmer.zaman.now.kotlin.restful.entity.ApiKey
import programmer.zaman.now.kotlin.restful.entity.Customer
import programmer.zaman.now.kotlin.restful.repository.ApiKeyRepository
import programmer.zaman.now.kotlin.restful.repository.CustomerRepository
import java.util.*

@Component
class ApiKeySeeder(val apiKeyRepository: ApiKeyRepository, val customerRepository: CustomerRepository) : ApplicationRunner {

    val apiKey = "SECRET"

    override fun run(args: ApplicationArguments?) {
        if (!apiKeyRepository.existsById(apiKey)) {
            val customer = Customer(
                firstName = "fendi",
                uuid = "customer-".plus(UUID.randomUUID().toString()),
                lastName = "fulan",
                phoneNumber = "09999",
                email = "fendi@titipbeliin.com",
                createdAt = Date(),
                updatedAt = null
            )
            customer.password = "1234"
            customerRepository.save(customer)
            val entity = ApiKey(id = apiKey, customer = customer, createdAt = Date(), updatedAt = null)
            apiKeyRepository.save(entity)
        }
    }
}