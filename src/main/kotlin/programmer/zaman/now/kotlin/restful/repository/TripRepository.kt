package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Trip
import org.springframework.data.jpa.repository.JpaRepository


interface TripRepository : JpaRepository<Trip, Long>
