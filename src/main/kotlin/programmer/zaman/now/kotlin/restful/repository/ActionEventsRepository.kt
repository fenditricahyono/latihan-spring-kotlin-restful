package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ActionEvents
import org.springframework.data.jpa.repository.JpaRepository


interface ActionEventsRepository : JpaRepository<ActionEvents, Long>
