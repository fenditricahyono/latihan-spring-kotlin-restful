package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.UserCashDeposit
import org.springframework.data.jpa.repository.JpaRepository


interface UserCashDepositRepository : JpaRepository<UserCashDeposit, Long>
