package programmer.zaman.now.kotlin.restful.model.request

data class OtoPaginationRequestList(

        val page: Int,

        val size: Int
)