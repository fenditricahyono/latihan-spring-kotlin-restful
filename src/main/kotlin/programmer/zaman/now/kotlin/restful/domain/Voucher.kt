package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Voucher {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 100)
    var voucherTitle: String? = null

    @Column(length = 32)
    var voucherCode: String? = null

    @Column
    var usedCount: Int? = null

    @Column
    var activeAt: OffsetDateTime? = null

    @Column
    var expiredAt: OffsetDateTime? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var isPublic: Boolean? = null

    @Column(nullable = false)
    var voucherValue: Int? = null

    @Column(nullable = false)
    var image: String? = null

    @Column(nullable = false)
    var voucherDescription: String? = null

    @Column(nullable = false)
    var voucherLimit: String? = null

    @Column
    var departureStartAt: OffsetDateTime? = null

    @Column
    var departureEndAt: OffsetDateTime? = null

    @Column
    var bookingStartAt: OffsetDateTime? = null

    @Column
    var bookingEndAt: OffsetDateTime? = null

    @Column(
        nullable = false,
        length = 500
    )
    var voucherImageUrl: String? = null

    @OneToMany(mappedBy = "voucher")
    var voucherInvoices: MutableSet<Invoice>? = null

    @OneToMany(mappedBy = "voucher")
    var voucherVoucherBusess: MutableSet<VoucherBuses>? = null

    @OneToMany(mappedBy = "voucher")
    var voucherVoucherBusNumberss: MutableSet<VoucherBusNumbers>? = null

    @OneToMany(mappedBy = "voucher")
    var voucherVoucherTripss: MutableSet<VoucherTrips>? = null

    @OneToMany(mappedBy = "voucher")
    var voucherVoucherUseds: MutableSet<VoucherUsed>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
