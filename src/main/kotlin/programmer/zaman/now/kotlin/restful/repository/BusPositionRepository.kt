package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusPosition
import org.springframework.data.jpa.repository.JpaRepository


interface BusPositionRepository : JpaRepository<BusPosition, Long>
