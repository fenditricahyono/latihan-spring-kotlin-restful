package programmer.zaman.now.kotlin.restful

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class KotlinRestfulApiApplication

fun main(args: Array<String>) {
	runApplication<KotlinRestfulApiApplication>(*args)
}
