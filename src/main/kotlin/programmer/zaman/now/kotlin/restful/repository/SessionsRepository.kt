package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Sessions
import org.springframework.data.jpa.repository.JpaRepository


interface SessionsRepository : JpaRepository<Sessions, String>
