package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BookingTripTrackSeat
import org.springframework.data.jpa.repository.JpaRepository


interface BookingTripTrackSeatRepository : JpaRepository<BookingTripTrackSeat, Long>
