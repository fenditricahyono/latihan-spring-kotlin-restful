package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TripSubroute
import org.springframework.data.jpa.repository.JpaRepository


interface TripSubrouteRepository : JpaRepository<TripSubroute, Long>
