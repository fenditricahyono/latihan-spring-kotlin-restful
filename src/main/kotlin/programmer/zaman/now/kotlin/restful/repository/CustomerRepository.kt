package programmer.zaman.now.kotlin.restful.repository

import org.springframework.data.jpa.repository.JpaRepository
import programmer.zaman.now.kotlin.restful.entity.Customer

interface CustomerRepository:JpaRepository<Customer, String> {
    fun findByEmail(email: String): Customer?
    fun findByUuid(uuid: String): Customer?
}