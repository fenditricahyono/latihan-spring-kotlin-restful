package programmer.zaman.now.kotlin.restful.service

import programmer.zaman.now.kotlin.restful.model.request.*
import programmer.zaman.now.kotlin.restful.model.response.CityResponse
import programmer.zaman.now.kotlin.restful.model.response.ProductResponse

interface CityService {

    fun create(cityRequest: CityRequest): CityResponse

    fun get(id: String): CityResponse

    fun update(id: String, cityRequest: CityRequest): CityResponse

    fun delete(id: String): Boolean

    fun list(otoPaginationRequestList: OtoPaginationRequestList): List<CityResponse>
}