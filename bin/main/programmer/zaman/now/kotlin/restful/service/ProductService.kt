package programmer.zaman.now.kotlin.restful.service

import programmer.zaman.now.kotlin.restful.model.request.ProductRequestCreate
import programmer.zaman.now.kotlin.restful.model.request.ProductRequestList
import programmer.zaman.now.kotlin.restful.model.response.ProductResponse
import programmer.zaman.now.kotlin.restful.model.request.UpdateProductRequest

interface ProductService {

    fun create(createProductRequest: ProductRequestCreate): ProductResponse

    fun get(id: String): ProductResponse

    fun update(id: String, updateProductRequest: UpdateProductRequest): ProductResponse

    fun delete(id: String)

    fun list(listProductRequest: ProductRequestList): List<ProductResponse>

}