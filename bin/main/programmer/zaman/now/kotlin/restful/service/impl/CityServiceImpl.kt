package programmer.zaman.now.kotlin.restful.service.impl

import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import programmer.zaman.now.kotlin.restful.entity.City
import programmer.zaman.now.kotlin.restful.entity.Product
import programmer.zaman.now.kotlin.restful.error.DuplicateException
import programmer.zaman.now.kotlin.restful.error.NotFoundException
import programmer.zaman.now.kotlin.restful.model.request.CityRequest
import programmer.zaman.now.kotlin.restful.model.request.OtoPaginationRequestList
import programmer.zaman.now.kotlin.restful.model.response.CityResponse
import programmer.zaman.now.kotlin.restful.model.response.ProductResponse
import programmer.zaman.now.kotlin.restful.model.response.ProductSpecificationResponse
import programmer.zaman.now.kotlin.restful.repository.CityRepository
import programmer.zaman.now.kotlin.restful.service.CityService
import programmer.zaman.now.kotlin.restful.validation.ValidationUtil
import java.util.*
import java.util.stream.Collectors

@Service
class CityServiceImpl(
    val cityRepository: CityRepository,
    val validationUtil: ValidationUtil
): CityService {

    override fun create(cityRequest: CityRequest): CityResponse {
        validationUtil.validate(cityRequest)
        val cityCheck = cityRepository.findByIdOrNull(cityRequest.code)
        if(cityCheck != null){
            throw DuplicateException(errorMessage = String.format("Kode sudah dipakai oleh kota %s", cityCheck.cityName))
        }
        val city = City(
            uuid = "city-".plus(UUID.randomUUID().toString()),
            code = cityRequest.code,
            cityName = cityRequest.city_name,
            createdAt = Date(),
            updatedAt = null,
            deletedAt = null
        )
        cityRepository.save(city)

        return convertCityResponse(city)
    }

    override fun get(code: String): CityResponse {
        val city = cityRepository.findByIdOrNull(code) ?: throw NotFoundException()
        return convertCityResponse(city)
    }

    override fun update(code: String, cityRequest: CityRequest): CityResponse {
        val city = cityRepository.findByIdOrNull(code) ?: throw NotFoundException()
        city.apply {
            cityName = cityRequest.city_name
            updatedAt = Date()
        }
        cityRepository.save(city)
        return convertCityResponse(city)
    }

    override fun delete(code: String): Boolean {
        val city = cityRepository.findByIdOrNull(code) ?: throw NotFoundException()
        try {
            cityRepository.delete(city)
            return true
        }catch (e: Exception){
            city.apply {
                deletedAt = Date()
            }
            cityRepository.save(city)
            return true
        }
        return false
    }

    override fun list(otoPaginationRequestList: OtoPaginationRequestList): List<CityResponse> {
        val page = cityRepository.findAll(PageRequest.of(otoPaginationRequestList.page, otoPaginationRequestList.size))
        val cities: List<City> = page.get().collect(Collectors.toList())
        return cities.map { convertCityResponse(it) }
    }

    private fun convertCityResponse(city: City): CityResponse {
        return CityResponse(
            uuid = city.uuid,
            code = city.code,
            city_name = city.cityName,
            created_at = city.createdAt,
            updated_at = city.updatedAt,
            deleted_at = city.deletedAt
        )
    }
}