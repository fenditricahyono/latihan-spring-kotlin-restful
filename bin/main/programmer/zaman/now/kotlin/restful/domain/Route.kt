package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Route {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 150)
    var routeName: String? = null

    @Column(name = "\"description\"")
    var description: String? = null

    @Column
    var distance: Int? = null

    @Column
    var differentTimeLeaveMinute: Int? = null

    @Column
    var differentTimeArrivedMinute: Int? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(columnDefinition = "longtext")
    var fileUploadExcel: String? = null

    @Column
    var sampleDepartureAt: OffsetDateTime? = null

    @Column
    var sampleArrivedAt: OffsetDateTime? = null

    @OneToMany(mappedBy = "route")
    var routeRouteBoardingPoints: MutableSet<RouteBoardingPoint>? = null

    @OneToMany(mappedBy = "route")
    var routeRouteClassPrices: MutableSet<RouteClassPrice>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    var parent: Route? = null

    @OneToMany(mappedBy = "parent")
    var parentRoutes: MutableSet<Route>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_city_id")
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_city_id")
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    var parent: Route? = null

    @OneToMany(mappedBy = "parent")
    var parentRoutes: MutableSet<Route>? = null

    @OneToMany(mappedBy = "route")
    var routeRoutePoints: MutableSet<RoutePoint>? = null

    @OneToMany(mappedBy = "route")
    var routeRouteTracks: MutableSet<RouteTrack>? = null

    @OneToMany(mappedBy = "route")
    var routeRouteTracks: MutableSet<RouteTrack>? = null

    @OneToMany(mappedBy = "route")
    var routeTicketPriceSchedulings: MutableSet<TicketPriceScheduling>? = null

    @OneToMany(mappedBy = "route")
    var routeTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "route")
    var routeTripSubroutes: MutableSet<TripSubroute>? = null

    @OneToMany(mappedBy = "route")
    var routeTripTemplates: MutableSet<TripTemplate>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
