package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Booking {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(unique = true)
    var bookingNumber: Long? = null

    @Column(
        unique = true,
        length = 50
    )
    var bookingCode: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var customerName: String? = null

    @Column(length = 50)
    var customerPhone: String? = null

    @Column(length = 100)
    var customerEmail: String? = null

    @Column(nullable = false)
    var totalPassenger: Int? = null

    @Column(nullable = false)
    var totalBill: Int? = null

    @Column(nullable = false)
    var totalRefund: Int? = null

    @Column(nullable = false)
    var totalRefundCharge: Int? = null

    @Column
    var boardingPoint: String? = null

    @Column
    var departureAddress: String? = null

    @Column
    var dropPoint: String? = null

    @Column
    var destinationAddress: String? = null

    @Column
    var agencyCommision: Int? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var additionalInfo: String? = null

    @Column(
        unique = true,
        length = 100
    )
    var accessToken: String? = null

    @Column(nullable = false)
    var expiredAt: OffsetDateTime? = null

    @Column(nullable = false)
    var isEditPrices: Boolean? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var commissionEachSeatIdr: Int? = null

    @Column(nullable = false)
    var commissionEachSeatPercent: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agency_code_id")
    var agencyCode: Agency? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    var customer: Customer? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_city_id")
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_city_id")
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    var invoice: Invoice? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_id")
    var `operator`: Users? = null

    @OneToMany(mappedBy = "booking")
    var bookingBookingPassengers: MutableSet<BookingPassenger>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_id")
    var trip: Trip? = null

    @OneToMany(mappedBy = "booking")
    var bookingBookingTripTrackSeats: MutableSet<BookingTripTrackSeat>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
