package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.OneToMany
import java.time.LocalDate
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Customer {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 100)
    var customerName: String? = null

    @Column(
        unique = true,
        length = 50
    )
    var customerPhone: String? = null

    @Column(
        unique = true,
        length = 50
    )
    var customerEmail: String? = null

    @Column
    var password: String? = null

    @Column
    var verificationCode: String? = null

    @Column
    var isVerifiedEmail: Boolean? = null

    @Column
    var isVerifiedPhoneNumber: Boolean? = null

    @Column
    var fcmId: String? = null

    @Column(unique = true)
    var facebookId: String? = null

    @Column(unique = true)
    var googleId: String? = null

    @Column
    var birthDate: LocalDate? = null

    @Column
    var cargoMember: Boolean? = null

    @Column
    var avatar: String? = null

    @Column(nullable = false)
    var rememberToken: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column
    var appleId: String? = null

    @OneToMany(mappedBy = "customer")
    var customerBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "customer")
    var customerCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "customer")
    var customerInvoices: MutableSet<Invoice>? = null

    @OneToMany(mappedBy = "customer")
    var customerVoucherUseds: MutableSet<VoucherUsed>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
