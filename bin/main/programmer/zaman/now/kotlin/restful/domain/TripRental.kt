package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class TripRental {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        length = 50
    )
    var uuid: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var departureCity: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var destinationCity: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var customerName: String? = null

    @Column(
        nullable = false,
        length = 200
    )
    var customerAddress: String? = null

    @Column(
        nullable = false,
        length = 20
    )
    var customerPhoneNumber: String? = null

    @Column
    var distance: Int? = null

    @Column(
        nullable = false,
        length = 100
    )
    var rentalPrice: String? = null

    @Column(
        nullable = false,
        name = "\"description\""
    )
    var description: String? = null

    @Column
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "trip_id",
        nullable = false
    )
    var trip: Trip? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
