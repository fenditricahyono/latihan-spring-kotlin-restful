package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class TicketPriceScheduling {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 50
    )
    var uuid: String? = null

    @Column(nullable = false)
    var activeAt: OffsetDateTime? = null

    @Column(nullable = false)
    var price: Int? = null

    @Column
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "departure_city_id",
        nullable = false
    )
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "destination_city_id",
        nullable = false
    )
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "operator_id",
        nullable = false
    )
    var `operator`: Users? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "bus_number_id",
        nullable = false
    )
    var busNumber: BusNumber? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "class_code_id",
        nullable = false
    )
    var classCode: SeatClass? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "route_id",
        nullable = false
    )
    var route: Route? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_price_scheduling_group_id")
    var ticketPriceSchedulingGroup: TicketPriceSchedulingGroup? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
