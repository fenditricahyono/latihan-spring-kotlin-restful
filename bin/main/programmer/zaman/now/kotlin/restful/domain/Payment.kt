package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Payment {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column
    var bankId: Long? = null

    @Column(
        nullable = false,
        unique = true
    )
    var paymentNumber: Long? = null

    @Column(
        nullable = false,
        unique = true,
        length = 50
    )
    var paymentCode: String? = null

    @Column
    var paymentValue: Int? = null

    @Column
    var paymentNote: String? = null

    @Column(length = 100)
    var bankName: String? = null

    @Column(length = 100)
    var bankAccountName: String? = null

    @Column(length = 50)
    var bankAccountNumber: String? = null

    @Column(length = 100)
    var bankBranch: String? = null

    @Column(
        nullable = false,
        length = 30
    )
    var bankCardNumber: String? = null

    @Column
    var paymentReceiptFile: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var paymentJson: String? = null

    @Column(nullable = false)
    var paymentAt: OffsetDateTime? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column(length = 30)
    var paymentTransactionNumber: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agency_code_id")
    var agencyCode: Agency? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    var invoice: Invoice? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_id")
    var `operator`: Users? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
