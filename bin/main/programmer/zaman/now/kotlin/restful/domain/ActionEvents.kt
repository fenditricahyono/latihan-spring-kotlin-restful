package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import java.time.OffsetDateTime
import java.util.UUID
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class ActionEvents {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        columnDefinition = "char(36)"
    )
    var batchId: UUID? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var name: String? = null

    @Column
    var note: String? = null

    @Column(nullable = false)
    var actionableType: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var actionableId: String? = null

    @Column(nullable = false)
    var targetType: String? = null

    @Column(
        nullable = false,
        length = 100
    )
    var targetId: String? = null

    @Column(nullable = false)
    var modelType: String? = null

    @Column(length = 100)
    var modelId: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var fields: String? = null

    @Column(
        nullable = false,
        length = 25
    )
    var status: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var exception: String? = null

    @Column
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column(columnDefinition = "longtext")
    var original: String? = null

    @Column(columnDefinition = "longtext")
    var changes: String? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
