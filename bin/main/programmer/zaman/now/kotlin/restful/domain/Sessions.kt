package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.Id
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Sessions {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    var id: String? = null

    @Column
    var userId: Long? = null

    @Column(length = 45)
    var ipAddress: String? = null

    @Column(columnDefinition = "longtext")
    var userAgent: String? = null

    @Column(
        nullable = false,
        columnDefinition = "longtext"
    )
    var payload: String? = null

    @Column(nullable = false)
    var lastActivity: Int? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
