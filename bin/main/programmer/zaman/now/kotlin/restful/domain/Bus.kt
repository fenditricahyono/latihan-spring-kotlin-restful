package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Bus {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(
        unique = true,
        length = 50
    )
    var nopol: String? = null

    @Column
    var km: Int? = null

    @Column
    var totalWindows: Int? = null

    @Column
    var facilities: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_type_id")
    var busType: BusType? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_number_id")
    var busNumber: BusNumber? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id")
    var driver: BusDriver? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id2_id")
    var driverId2: BusDriver? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conductor_id")
    var conductor: BusConductor? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conductor_id2_id")
    var conductorId2: BusConductor? = null

    @OneToMany(mappedBy = "bus")
    var busBusPositions: MutableSet<BusPosition>? = null

    @OneToMany(mappedBy = "bus")
    var busExpenses: MutableSet<Expense>? = null

    @OneToMany(mappedBy = "bus")
    var busTripCosts: MutableSet<TripCost>? = null

    @OneToMany(mappedBy = "bus")
    var busTrips: MutableSet<Trip>? = null

    @OneToMany(mappedBy = "bus")
    var busVoucherBusess: MutableSet<VoucherBuses>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
