package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Cargo {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 50)
    var trackingNumber: String? = null

    @Column(length = 150)
    var senderName: String? = null

    @Column(length = 150)
    var senderPhoneNumber: String? = null

    @Column(length = 150)
    var senderAddress: String? = null

    @Column(length = 150)
    var recipientName: String? = null

    @Column(length = 150)
    var recipientPhoneNumber: String? = null

    @Column(length = 150)
    var recipientAddress: String? = null

    @Column
    var receiveAt: OffsetDateTime? = null

    @Column
    var isFragile: Boolean? = null

    @Column
    var isPriority: Boolean? = null

    @Column(length = 150)
    var note: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var totalBill: Int? = null

    @Column(nullable = false)
    var commissionEachCargoIdr: Int? = null

    @Column(nullable = false)
    var commissionEachCargoPercent: Int? = null

    @OneToMany(mappedBy = "cargo")
    var cargoCargoDetails: MutableSet<CargoDetail>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_id")
    var invoice: Invoice? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "trip_id",
        nullable = false
    )
    var trip: Trip? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    var customer: Customer? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "operator_create_id",
        nullable = false
    )
    var operatorCreate: Users? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "operator_receive_id")
    var operatorReceive: Users? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "agency_departure_id",
        nullable = false
    )
    var agencyDeparture: Agency? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "agency_destination_id",
        nullable = false
    )
    var agencyDestination: Agency? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
