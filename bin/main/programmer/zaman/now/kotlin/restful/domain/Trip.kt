package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.GeneratedValue
import jakarta.persistence.GenerationType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Trip {

    @Id
    @Column(
        nullable = false,
        updatable = false
    )
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @Column(
        nullable = false,
        unique = true,
        length = 50
    )
    var uuid: String? = null

    @Column(nullable = false)
    var departureTime: OffsetDateTime? = null

    @Column(nullable = false)
    var arrivedTime: OffsetDateTime? = null

    @Column
    var distance: Int? = null

    @Column(nullable = false)
    var isOnline: Int? = null

    @Column(nullable = false)
    var totalIncomeCargo: Int? = null

    @Column(nullable = false)
    var totalIncomeRefund: Int? = null

    @Column(nullable = false)
    var totalIncomeTicket: Int? = null

    @Column(nullable = false)
    var totalPassenger: Int? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var totalIncomeRental: Long? = null

    @OneToMany(mappedBy = "trip")
    var tripBookingPassengers: MutableSet<BookingPassenger>? = null

    @OneToMany(mappedBy = "trip")
    var tripBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "trip")
    var tripBookingTripTrackSeats: MutableSet<BookingTripTrackSeat>? = null

    @OneToMany(mappedBy = "trip")
    var tripCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "trip")
    var tripTripCosts: MutableSet<TripCost>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trip_template_id")
    var tripTemplate: TripTemplate? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conductor_id2_id")
    var conductorId2: BusConductor? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_type_id")
    var busType: BusType? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "bus_id",
        nullable = false
    )
    var bus: Bus? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "bus_number_id",
        nullable = false
    )
    var busNumber: BusNumber? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "departure_city_id")
    var departureCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "destination_city_id")
    var destinationCity: City? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id")
    var driver: BusDriver? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id2_id")
    var driverId2: BusDriver? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conductor_id")
    var conductor: BusConductor? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "bus_layout_id",
        nullable = false
    )
    var busLayout: BusLayout? = null

    @OneToMany(mappedBy = "trip")
    var tripTripRentals: MutableSet<TripRental>? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "route_id")
    var route: Route? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bus_layout_second_id")
    var busLayoutSecond: BusLayout? = null

    @OneToMany(mappedBy = "trip")
    var tripTripSubroutes: MutableSet<TripSubroute>? = null

    @OneToMany(mappedBy = "trip")
    var tripTripTracks: MutableSet<TripTrack>? = null

    @OneToMany(mappedBy = "trip")
    var tripVoucherTripss: MutableSet<VoucherTrips>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
