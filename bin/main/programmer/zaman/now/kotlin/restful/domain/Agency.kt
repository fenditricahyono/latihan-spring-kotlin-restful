package programmer.zaman.now.kotlin.restful.domain

import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EntityListeners
import jakarta.persistence.FetchType
import jakarta.persistence.Id
import jakarta.persistence.JoinColumn
import jakarta.persistence.ManyToOne
import jakarta.persistence.OneToMany
import java.time.OffsetDateTime
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener


@Entity
@EntityListeners(AuditingEntityListener::class)
class Agency {

    @Id
    @Column(
        nullable = false,
        updatable = false,
        length = 20
    )
    var code: String? = null

    @Column(
        unique = true,
        length = 100
    )
    var uuid: String? = null

    @Column(length = 150)
    var agencyName: String? = null

    @Column(nullable = false)
    var createdAt: OffsetDateTime? = null

    @Column
    var updatedAt: OffsetDateTime? = null

    @Column
    var deletedAt: OffsetDateTime? = null

    @Column(nullable = false)
    var commissionEachSeatIdr: Int? = null

    @Column(nullable = false)
    var commissionEachSeatPercent: Int? = null

    @Column(nullable = false)
    var commissionEachCargoIdr: Int? = null

    @Column(nullable = false)
    var commissionEachCargoPercent: Int? = null

    @Column
    var limitPassenger: Int? = null

    @Column
    var limitPerTrip: Int? = null

    @Column
    var limitPerTrack: Int? = null

    @Column(
        nullable = false,
        length = 150
    )
    var address: String? = null

    @Column
    var tempCity: String? = null

    @Column(
        nullable = false,
        length = 50
    )
    var phoneNumber: String? = null

    @Column(nullable = false)
    var isShowContact: Boolean? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_code_id")
    var cityCode: City? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodeBanks: MutableSet<Bank>? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodeBookings: MutableSet<Booking>? = null

    @OneToMany(mappedBy = "agencyDeparture")
    var agencyDepartureCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "agencyDestination")
    var agencyDestinationCargos: MutableSet<Cargo>? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodePayments: MutableSet<Payment>? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodeUserss: MutableSet<Users>? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodeUserCashDeposits: MutableSet<UserCashDeposit>? = null

    @OneToMany(mappedBy = "agencyCode")
    var agencyCodeUserCashHistorys: MutableSet<UserCashHistory>? = null

    @CreatedDate
    @Column(
        nullable = false,
        updatable = false
    )
    var dateCreated: OffsetDateTime? = null

    @LastModifiedDate
    @Column(nullable = false)
    var lastUpdated: OffsetDateTime? = null

}
