package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Booking
import org.springframework.data.jpa.repository.JpaRepository


interface BookingRepository : JpaRepository<Booking, Long>
