package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Voucher
import org.springframework.data.jpa.repository.JpaRepository


interface VoucherRepository : JpaRepository<Voucher, Long>
