package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Upload
import org.springframework.data.jpa.repository.JpaRepository


interface UploadRepository : JpaRepository<Upload, Int>
