package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.FailedJobs
import org.springframework.data.jpa.repository.JpaRepository


interface FailedJobsRepository : JpaRepository<FailedJobs, Long>
