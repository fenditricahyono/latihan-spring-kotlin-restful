package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.OtoDate
import org.springframework.data.jpa.repository.JpaRepository


interface OtoDateRepository : JpaRepository<OtoDate, Long>
