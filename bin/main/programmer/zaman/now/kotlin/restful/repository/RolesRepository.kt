package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Roles
import org.springframework.data.jpa.repository.JpaRepository


interface RolesRepository : JpaRepository<Roles, Long>
