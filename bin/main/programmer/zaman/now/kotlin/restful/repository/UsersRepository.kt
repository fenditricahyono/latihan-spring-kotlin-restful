package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Users
import org.springframework.data.jpa.repository.JpaRepository


interface UsersRepository : JpaRepository<Users, Long>
