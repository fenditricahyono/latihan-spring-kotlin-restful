package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Cargo
import org.springframework.data.jpa.repository.JpaRepository


interface CargoRepository : JpaRepository<Cargo, Long>
