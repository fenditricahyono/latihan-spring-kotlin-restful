package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.RouteClassPrice
import org.springframework.data.jpa.repository.JpaRepository


interface RouteClassPriceRepository : JpaRepository<RouteClassPrice, Long>
