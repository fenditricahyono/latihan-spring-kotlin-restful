package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusDriver
import org.springframework.data.jpa.repository.JpaRepository


interface BusDriverRepository : JpaRepository<BusDriver, Long>
