package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.SeatClass
import org.springframework.data.jpa.repository.JpaRepository


interface SeatClassRepository : JpaRepository<SeatClass, String>
