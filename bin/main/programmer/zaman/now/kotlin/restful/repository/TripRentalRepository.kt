package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TripRental
import org.springframework.data.jpa.repository.JpaRepository


interface TripRentalRepository : JpaRepository<TripRental, Long>
