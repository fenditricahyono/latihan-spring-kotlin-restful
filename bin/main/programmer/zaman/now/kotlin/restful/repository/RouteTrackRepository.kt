package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.RouteTrack
import org.springframework.data.jpa.repository.JpaRepository


interface RouteTrackRepository : JpaRepository<RouteTrack, Long>
