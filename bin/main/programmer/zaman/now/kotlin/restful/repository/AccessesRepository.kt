package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Accesses
import org.springframework.data.jpa.repository.JpaRepository


interface AccessesRepository : JpaRepository<Accesses, Long>
