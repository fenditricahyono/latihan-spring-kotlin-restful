package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.UserCashHistory
import org.springframework.data.jpa.repository.JpaRepository


interface UserCashHistoryRepository : JpaRepository<UserCashHistory, Long>
