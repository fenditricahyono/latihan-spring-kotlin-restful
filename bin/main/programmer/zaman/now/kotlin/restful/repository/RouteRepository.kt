package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Route
import org.springframework.data.jpa.repository.JpaRepository


interface RouteRepository : JpaRepository<Route, Long>
