package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.PersonalAccessTokens
import org.springframework.data.jpa.repository.JpaRepository


interface PersonalAccessTokensRepository : JpaRepository<PersonalAccessTokens, Long>
