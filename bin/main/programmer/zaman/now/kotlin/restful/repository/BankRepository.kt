package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Bank
import org.springframework.data.jpa.repository.JpaRepository


interface BankRepository : JpaRepository<Bank, Long>
