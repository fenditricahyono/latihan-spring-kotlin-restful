package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Migrations
import org.springframework.data.jpa.repository.JpaRepository


interface MigrationsRepository : JpaRepository<Migrations, Int>
