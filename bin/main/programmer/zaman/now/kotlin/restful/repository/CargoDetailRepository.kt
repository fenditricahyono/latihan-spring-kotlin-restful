package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.CargoDetail
import org.springframework.data.jpa.repository.JpaRepository


interface CargoDetailRepository : JpaRepository<CargoDetail, Long>
