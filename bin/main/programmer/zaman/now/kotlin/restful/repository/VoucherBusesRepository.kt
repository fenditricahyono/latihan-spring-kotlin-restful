package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.VoucherBuses
import org.springframework.data.jpa.repository.JpaRepository


interface VoucherBusesRepository : JpaRepository<VoucherBuses, Long>
