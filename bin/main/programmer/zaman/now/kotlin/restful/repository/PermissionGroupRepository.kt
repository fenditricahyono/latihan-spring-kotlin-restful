package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.PermissionGroup
import org.springframework.data.jpa.repository.JpaRepository


interface PermissionGroupRepository : JpaRepository<PermissionGroup, Int>
