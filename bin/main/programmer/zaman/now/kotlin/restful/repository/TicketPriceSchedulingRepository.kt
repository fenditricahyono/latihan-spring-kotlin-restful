package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TicketPriceScheduling
import org.springframework.data.jpa.repository.JpaRepository


interface TicketPriceSchedulingRepository : JpaRepository<TicketPriceScheduling, Long>
