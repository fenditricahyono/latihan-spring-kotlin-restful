package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TripCost
import org.springframework.data.jpa.repository.JpaRepository


interface TripCostRepository : JpaRepository<TripCost, Long>
