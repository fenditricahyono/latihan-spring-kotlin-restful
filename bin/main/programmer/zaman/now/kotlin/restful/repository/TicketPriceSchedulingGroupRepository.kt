package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.TicketPriceSchedulingGroup
import org.springframework.data.jpa.repository.JpaRepository


interface TicketPriceSchedulingGroupRepository : JpaRepository<TicketPriceSchedulingGroup, Long>
