package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ModelHasRoles
import org.springframework.data.jpa.repository.JpaRepository


interface ModelHasRolesRepository : JpaRepository<ModelHasRoles, String>
