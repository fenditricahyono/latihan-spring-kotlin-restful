package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Logs
import org.springframework.data.jpa.repository.JpaRepository


interface LogsRepository : JpaRepository<Logs, Long>
