package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Invoice
import org.springframework.data.jpa.repository.JpaRepository


interface InvoiceRepository : JpaRepository<Invoice, Long>
