package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.InvoiceLog
import org.springframework.data.jpa.repository.JpaRepository


interface InvoiceLogRepository : JpaRepository<InvoiceLog, Long>
