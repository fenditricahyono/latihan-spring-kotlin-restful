package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusLayout
import org.springframework.data.jpa.repository.JpaRepository


interface BusLayoutRepository : JpaRepository<BusLayout, Long>
