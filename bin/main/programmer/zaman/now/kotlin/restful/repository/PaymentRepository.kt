package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Payment
import org.springframework.data.jpa.repository.JpaRepository


interface PaymentRepository : JpaRepository<Payment, Long>
