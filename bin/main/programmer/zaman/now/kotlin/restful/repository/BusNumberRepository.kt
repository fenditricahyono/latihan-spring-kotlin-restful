package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BusNumber
import org.springframework.data.jpa.repository.JpaRepository


interface BusNumberRepository : JpaRepository<BusNumber, Long>
