package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ExpensePayment
import org.springframework.data.jpa.repository.JpaRepository


interface ExpensePaymentRepository : JpaRepository<ExpensePayment, Long>
