package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.Otps
import org.springframework.data.jpa.repository.JpaRepository


interface OtpsRepository : JpaRepository<Otps, Int>
