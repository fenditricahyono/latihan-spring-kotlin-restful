package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ActivityLog
import org.springframework.data.jpa.repository.JpaRepository


interface ActivityLogRepository : JpaRepository<ActivityLog, Long>
