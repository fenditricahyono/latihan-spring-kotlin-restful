package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.ModelHasPermissions
import org.springframework.data.jpa.repository.JpaRepository


interface ModelHasPermissionsRepository : JpaRepository<ModelHasPermissions, String>
