package programmer.zaman.now.kotlin.restful.repository

import id.otobus.otobus_backend.domain.BoardingPoint
import org.springframework.data.jpa.repository.JpaRepository


interface BoardingPointRepository : JpaRepository<BoardingPoint, Long>
