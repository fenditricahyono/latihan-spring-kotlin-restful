package programmer.zaman.now.kotlin.restful.model.request

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class ProductSpecificationRequest(

        @field:NotBlank
        val key: String?,

        @field:NotBlank
        val value: String?
)