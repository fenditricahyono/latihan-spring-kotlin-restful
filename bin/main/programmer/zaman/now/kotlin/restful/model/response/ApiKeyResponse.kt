package programmer.zaman.now.kotlin.restful.model.response

import java.util.*

data class ApiKeyResponse(
    val token: String,
    var createdAt: Date
)