package programmer.zaman.now.kotlin.restful.model.response

import java.util.Date

data class CityResponse(
    val uuid: String,
    val code: String,
    val city_name: String,
    val created_at: Date,
    val updated_at: Date?,
    val deleted_at: Date?
)
