package programmer.zaman.now.kotlin.restful.model.response

import java.util.*

data class UserResponse(

        val id: Int,

        val username: String,

        val fullname: String,

        val createdAt: Date,

        val updatedAt: Date?

)