package programmer.zaman.now.kotlin.restful.model.response

data class ProductSpecificationResponse(

        val id: Int,

        val key: String,

        val value: String

)