package programmer.zaman.now.kotlin.restful.error

class UnauthorizedException(val errorMessage: String?) : Exception() {
}