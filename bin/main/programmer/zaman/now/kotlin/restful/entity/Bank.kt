package programmer.zaman.now.kotlin.restful.entity

import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "bank")
data class Bank(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val code: Int? = null,

    @Column(name = "bank_name")
    var bankName: String,
    
    @Column(name = "account_number", length = 75, unique = true)
    val accountNumber: String,

    @Column(name = "account_name")
    var accountName: String,

    @ManyToOne
    @JoinColumn(name = "agency_code", nullable = true)
    var agency: Agency,

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    val createdAt: Date,

    @Column(name = "updated_at", columnDefinition = "timestamp with time zone")
    var updatedAt: Date?,

    @Column(name = "deleted_at", insertable = false, columnDefinition = "timestamp with time zone")
    var deletedAt: Date?
)