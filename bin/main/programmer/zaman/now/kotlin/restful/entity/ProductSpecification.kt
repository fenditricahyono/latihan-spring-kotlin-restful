package programmer.zaman.now.kotlin.restful.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "product_specification")
data class ProductSpecification(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Int? = null,

        @Column(name = "key")
        var key: String,

        @Column(name = "value")
        var value: String,

        @ManyToOne
        @JoinColumn(name = "product_id")
        var product: Product
)