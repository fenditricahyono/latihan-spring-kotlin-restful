package programmer.zaman.now.kotlin.restful.entity;

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.Table


@Entity
@Table(name="bus")
data class Bus (

    @GeneratedValue
    @Id
    val id: Int? = null,

    @Column(length = 3, unique = true)
    val nopol:String,

    @Column
    val km: Int? = 0,

    @Column(name="driver_1")
    @ManyToOne
    val driver1: Driver,

    @Column(name="driver_2")
    @ManyToOne
    val driver2: Driver? = null,

    @Column(name="driver_asistance_1")
    @ManyToOne
    val driverAsistance1: DriverAsistance,

    @Column(name="driver_asistance_2")
    @ManyToOne
    val driverAsistance2: DriverAsistance? = null,

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    val createdAt: Date,

    @Column(name = "updated_at", columnDefinition = "timestamp with time zone")
    var updatedAt: Date?,

    @Column(name = "deleted_at", insertable = false, columnDefinition = "timestamp with time zone")
    var deletedAt: Date?


    )
