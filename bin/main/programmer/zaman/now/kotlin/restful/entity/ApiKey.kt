package programmer.zaman.now.kotlin.restful.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "api_keys")
data class ApiKey(

        @Id
        val id: String,

        @ManyToOne
        @JoinColumn(name = "customer_id")
        var customer: Customer,

        @Column(name = "created_at")
        var createdAt: Date = Date(),

        @Column(name = "updated_at")
        var updatedAt: Date? = null

)