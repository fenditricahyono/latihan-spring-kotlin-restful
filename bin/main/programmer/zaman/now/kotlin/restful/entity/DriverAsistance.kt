package programmer.zaman.now.kotlin.restful.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name="driver")
data class DriverAsistance(

    @Id
    @GeneratedValue
    val id: Int?=null,

    @Column(name = "asistance_name", length = 60)
    val driverName: String,

    @Column(name = "phone_number", length = 15)
    val phoneNumber: String,

    @Column(length=255)
    val address: String?=null,

    @Column(name = "created_at", columnDefinition = "timestamp with time zone")
    val createdAt: Date,

    @Column(name = "updated_at", columnDefinition = "timestamp with time zone")
    var updatedAt: Date?,

    @Column(name = "deleted_at", insertable = false, columnDefinition = "timestamp with time zone")
    var deletedAt: Date?
)
