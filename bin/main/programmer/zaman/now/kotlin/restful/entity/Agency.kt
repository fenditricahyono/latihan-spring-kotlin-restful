package programmer.zaman.now.kotlin.restful.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
import javax.persistence.Table

@Entity
@Table(name = "agency")
data class Agency(

    @Id
    @Column(name = "code", length = 10)
    val code: String,

    @Column(name = "uuid", length = 50, unique = true)
    val uuid: String,

    @Column(name = "agency_name", length = 32)
    val agencyName: String,

    @ManyToOne
    @JoinColumn(name = "city_code")
    val city: City,

    @Column(name = "commission_each_cargo_idr")
    val commissionEachCargoIdr: Int,

    @Column(name = "commission_each_cargo_percent")
    val commissionEachCargoPercent: Int,

    @Column(name = "commission_each_seat_idr")
    val commissionEachSeatIdr: Int,

    @Column(name = "commission_each_seat_percent")
    val commissionEachSeatPercent: Int,

    @Column(name = "limit_passenger")
    val limitPassenger: Int,

    @Column(name = "limit_per_track")
    val limitPerTrack: Int,

    @Column(name = "limit_per_trip")
    val limitPerTrip: Int,

    @Column(name = "created_at")
    var createdAt: Date,

    @Column(name = "updated_at")
    var updatedAt: Date?,

    @Column(name = "deleted_at")
    var deletedAt: Date?
    )
